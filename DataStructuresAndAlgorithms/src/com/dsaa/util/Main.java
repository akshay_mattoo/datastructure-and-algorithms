package com.dsaa.util;

import com.dsaa.search.BinarySearch;
import com.dsaa.search.LinearSearch;
import com.dsaa.sort.BubbleSort;
import com.dsaa.sort.InsertionSort;
import com.dsaa.sort.MergeSort;
import com.dsaa.sort.SelectionSort;

public class Main {

	public static void main(String[] args) {
		DSAUtil util = new DSAUtil();
		int arr[] =util.generateArray();
		util.printArray(arr);
		
		/*LinearSearch ls = new LinearSearch();
		String str =ls.performLinearSearch(arr, 11);
		System.out.println(str);*/
		
		/*BubbleSort bs = new BubbleSort();
		arr = bs.performBubbleSort(arr);
		util.printArray(arr);
	*/
		
		/*BinarySearch bs = new BinarySearch();
		System.out.println(bs.performBinarySearch(arr,11));*/
		
		/*SelectionSort ss = new SelectionSort();
		arr = ss.performSelectionSort(arr);
		util.printArray(arr);*/
	/*	
		InsertionSort is = new InsertionSort();
		arr = is.performInsertionSort(arr);
		util.printArray(arr);*/
		
		MergeSort is = new MergeSort();
		arr = is.performMergeSort(arr);
		util.printArray(arr);
		
		/*Main m = new Main();
		m.foobar(4);*/
		
	}
	int foobar(int a)
	{
	System.out.println("foo"+a);
	return(a * foobar(a - 1));
	}
	
}
