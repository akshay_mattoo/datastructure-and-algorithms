package com.dsaa.util;

public class DSAUtil {

	int[] arrayTobeGenerated = new int[10];
	int arraySize=arrayTobeGenerated.length;
	
	public int[] generateArray()
	{
		for(int i=0;i<arraySize ;i++)
		{
			arrayTobeGenerated[i] = (int)(Math.random()*10)+10;
		}
		return arrayTobeGenerated;
	}
	
	
	public void printArray(int[] arr)
	{
		System.out.println("------");
		for(int i=0 ; i<arr.length;i++)
		{
			System.out.println("|"+i+"|"+arr[i]+"|");
			System.out.println("------");
		}
	}
}
