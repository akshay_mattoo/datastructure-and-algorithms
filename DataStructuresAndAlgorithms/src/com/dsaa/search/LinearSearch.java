package com.dsaa.search;

/**
 * 
 * @author Akshay
 *  In this we go though all the elements  in the array.
 */
public class LinearSearch {

	public String performLinearSearch(int[] arrayToBeSearched , int Value) {
		
		String returnIndexes="";
		boolean isPresent= false;
		for(int i=0 ; i<arrayToBeSearched.length;i++)
		{
			if(Value == arrayToBeSearched[i])
			{
				isPresent = true;
				returnIndexes+=i+" ";
			}
		}
		
		if(!isPresent)
		{
			returnIndexes="None";
		}
		return returnIndexes;
	}
}
