package com.dsaa.search;

/**
 * 
 * @author Akshay
 *This class is binary search as it keeps on halving the array size either on left side or right.
 *This 
 *Best case O(1)
 *worst case O(log N) . 
 *
 *   N-----N/2------N/4-------1    .. the values are getting halved till one so the complexity is N/2^k=1 i.e N=2^k i.e k = O(logN)
 */
public class BinarySearch {

	
	public int performBinarySearch(int[] arr , int value)
	{
		int flag=0;
		
		int start =0;
		int end =arr.length-1;
		
		
		while(start<=end)
		{
			int mid = (start+end)/2;
			if(value == arr[mid])
			{
				flag=1;
				return flag;
			}
			
			else if(value < arr[mid])
			{
				end = mid-1;
			}
			else if(value > arr[mid])
			{
				start =mid+1;
			}
			 
		}
		
		return -1;
	}
}
