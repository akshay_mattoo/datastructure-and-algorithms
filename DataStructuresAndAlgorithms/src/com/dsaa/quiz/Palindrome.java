package com.dsaa.quiz;

public class Palindrome {

	public static void main(String[] args) {
		
		String str = "live evil";
		System.out.println(isPalindrome(str));
	}
	
	static String removeSpecialCharacters(String string) {
	    int length = string.length();
	    StringBuffer bfr = new StringBuffer(length);
	   

	    for (int i = (length - 1); i >= 0; i--) {
	    	 char character = string.charAt(i);
	      if (Character.isLetterOrDigit(character)) {
	        bfr.append(character);
	      }
	    }

	    return bfr.toString();
	  }
	
	static String reverseString(String wordOrPhrase)
	{
		StringBuffer bfr = new StringBuffer(wordOrPhrase);
		bfr = bfr.reverse();
		return bfr.toString();
	}
	static boolean isPalindrome(String wordOrPhrase) {

		String cleanString = removeSpecialCharacters(wordOrPhrase);
		String reversedString = reverseString(cleanString);
		boolean flag=false;
		 
		
		if(cleanString.equalsIgnoreCase(reversedString))
		{
			flag =true;
		}
		return flag;
	}
}
