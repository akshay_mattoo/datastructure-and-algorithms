package com.dsaa.quiz;

public class ArmstrongCheck {

	public static void main(String[] args) {
		
		/*int number =1;
		
		System.out.println(number/10);
		System.out.println(number%10);*/
		
		System.out.println(isArmstrong(153));
	}
	
	static boolean isArmstrong(int number)
	{
		boolean flag= false;
		if(number == 3)
		{
			int result=0;
			int org = number;
			
			while(number!=0)
			{
				int remainder = number%10;
				result+=(remainder*remainder*remainder);
				number  = number/10;
			}
			
			if(org == result)
				flag= true;
		}
		return flag;
	}
}
