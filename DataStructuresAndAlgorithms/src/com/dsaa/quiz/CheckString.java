package com.dsaa.quiz;

import java.util.BitSet;

public class CheckString {

	public static void main(String[] args) {
		String s="qwertyuioplkjhgfdsazxcvbnm";
		System.out.println(findIfStringIsUnique(s));
	}
	static boolean findIfStringIsUnique(String sequence)
	{
		boolean flag = false;
	    BitSet alpha = new BitSet(26);
	    char letterArray[] =  sequence.toUpperCase().toCharArray();
	    for(char alphabet : letterArray)
	    {
	        if(Character.isLetter(alphabet))
	            alpha.set(alphabet - 65);
	    }

	    if(alpha.cardinality() == 26)
	    	flag = true;
	    return flag;
	}
}
