package com.dsaa.quiz;

public class StringReverse {

	
	
	public static void main(String[] args) {
		String str ="akshay";
		
		System.out.println(reverse(str));
		
	}
	
	 static boolean reverse(String s) {
		 String original = s;
		 boolean flag = false;
         int length = s.length();
         char[] arrayCh = s.toCharArray();
         for(int i=0; i< length/2; i++) {
           char ch = s.charAt(i);
           arrayCh[i] = arrayCh[length-1-i];
           arrayCh[length-1-i] = ch;            
         }

         if(original.equals(arrayCh.toString()))
         {
        	 flag = true;
         }
    return flag;
}
}
