package com.dsaa.quiz;

public class Factorial {

	public static void main(String[] args) {
		
		System.out.println(computeFactorial(5));
	}
	
	static int getFactorial(int number)
	{
		
		int result=1;
		
		if(number ==1 || number ==0)
			return result;
		if(number < 0)
			return 0;
		//result = getFactorial(number-1)*number;
		
		/*for(int i=number ; i>0;i--)
		{
			result*=i;
		}*/
		return result;
	}
	
	
	static int computeFactorial(int number) {
		int result=1;
		
		if(number ==1 || number ==0)
			return result;
		if(number < 0)
			return 0;
		
		for(int i=number ; i>0;i--)
		{
			result*=i;
		}
		return result;
    }
}
