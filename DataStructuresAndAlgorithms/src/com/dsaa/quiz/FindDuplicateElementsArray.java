package com.dsaa.quiz;

import java.util.HashMap;
import java.util.Map;

public class FindDuplicateElementsArray {

	
	public static void main(String[] args) {
		int arr[] = {2,3,5,6,2,6,8};
		
		Map<Integer, Integer> map = new HashMap<Integer,Integer>();
		
		for(int i=0;i<arr.length;i++)
		{
			 
			if(map.containsKey(arr[i]))
				map.put(arr[i],Integer.parseInt(map.get(arr[i]).toString())+1);
			else
				map.put(arr[i], 1);
		}
		
		System.out.println(map.toString());
		
	}
}
