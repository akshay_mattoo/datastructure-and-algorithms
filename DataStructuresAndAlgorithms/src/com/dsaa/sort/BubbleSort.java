package com.dsaa.sort;

import com.dsaa.util.DSAUtil;
/**
 * 
 * @author Akshay
 *This program is the bubble sort which sorts the program according to the order we specify.In bubble sort we compare the element with the value adjacent to it , if the value at the left side is larger than the value at the right hand side then we swap the values.
 *We go through this for the whole array.
 *Best case is O(n)
 *worst case is O(n2)
 *Average case is O(n2)
 *
 *Here we can improve the program by cutting down the second search as the element have already reached the last place needs not be taken in consideration for the search again and swapping.
 *And if the element is already sorted then O(1) is the best case.As only one pass is taken.
 *
 */
public class BubbleSort {

	public int[] performBubbleSort(int[] arr)
	{
		int counter=arr.length-1;
		int flag =0;
		for(int i=0;i<arr.length-1;i++)
		{
			
			for(int j=0;j<counter;j++)
			{
				if(arr[j]>arr[j+1])
				{
					flag=1;
					int tempval = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = tempval;
				}
				
			}
			if(flag == 1)
				counter--;
			if(flag==0)
			{
				System.out.println("break");
				break;
			}
			
		}
		
		return arr;
	}
	
	public static void main(String[] args) {
		int arr[] = {14,10,17,12,12,10,10,12,12,14};
				
		BubbleSort bs = new BubbleSort();
		arr=bs.performBubbleSort(arr);
		DSAUtil ut = new DSAUtil();
		ut.printArray(arr);
		
		
	}
	
}
