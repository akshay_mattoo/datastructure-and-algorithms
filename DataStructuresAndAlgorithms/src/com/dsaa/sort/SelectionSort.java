package com.dsaa.sort;

/**
 * 
 * @author Akshay
 *  This algorithm is a sorting algorithm .In this we take the first value as the minimum value and compare it all others and if we find the smaller value than that we take that value as minimum and place it at the first , basically we swap the values.
 *  We do this for the length of the array.We have two loops 
 *  Best case O(1).
 *  Worst Case O(n2).
 *  
 */
public class SelectionSort {

	public int[] performSelectionSort(int[] arr)
	{
		//Here we will run till n-2 because the last element will be sorted.
		for(int i=0 ; i<arr.length-1;i++)
		{
			//Minimum value is initilaised here because the value will change after the loop as the first value will be minimum
			int iMin =i;
			//This is to see the each elet with other element
			for(int j=i+1; j<arr.length;j++)
			{
				if(arr[j]<arr[iMin])
				{
					iMin =j;
				}
			}
			int temp = arr[i];
			arr[i] =arr[iMin];
			arr[iMin]=temp;
			
		}
		
		return arr;
	}
}
