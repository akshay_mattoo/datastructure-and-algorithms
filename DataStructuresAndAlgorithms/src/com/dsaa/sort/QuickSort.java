package com.dsaa.sort;

/**
 * @author Akshay
 * Quick sort algorithm works by taking a pivot element and partitining the array again and again.
 * O(n) = n log n -------in average case , O(n^2)------in worst case 
 * It is an in place algorithm , which means it doesnot require any extra place to store the elemts to be sorted.
 * 
 */
public class QuickSort {

	public static int[] performQuickSort(int[] arr,int start, int end)
	{
		if(start<end){
			int index=partitionElements(arr, start, end);
			performQuickSort(arr,start,index-1);
			performQuickSort(arr, index+1, end);
		}
		return arr;
	}
	
	private static int partitionElements(int a[],int startIndex, int endIndex)
	{
		int pivot = a[endIndex];
		int pIndex=startIndex;
		int temp;
		for(int i=startIndex;i<endIndex;i++){
			if(a[i]<=pivot){
				temp =a[i];
				a[i] = a[pIndex];
				a[pIndex] = temp;
				pIndex++;
			}
		}
		temp =a[pIndex];
		a[pIndex] = a[endIndex];
		a[endIndex] = temp;
		return pIndex; 
	}
	
}
