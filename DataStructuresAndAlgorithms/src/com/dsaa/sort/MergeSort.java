package com.dsaa.sort;

public class MergeSort {
	
	public int[] performMergeSort(int[] a)
	{
		if(a.length<2)
			return a;
		
		int mainLength=a.length;
		int leftLength = mainLength/2;
		int rightLength = mainLength-leftLength;
		int [] leftArray= new int[leftLength];
		int [] rightArray= new int[rightLength];
		
		for(int i=0;i<=leftLength-1;i++)
			leftArray[i] = a[i];

		for(int i=leftLength;i<mainLength;i++){
			rightArray[i-leftLength] = a[i];
		}
		performMergeSort(leftArray);
		performMergeSort(rightArray);
		
		merge(leftArray,rightArray,a);
		
		return a;
	}
	
	
	
	private static void merge(int []leftArray ,int []rightArray,int []mainArray){
		
		int nL  = leftArray.length;
		int nR  = rightArray.length;
//		int mlen  = mainArray.length; 
				
		int i=0,j=0,k=0;
		
		while(i<nL&& j<nR)
		{
			
			if(leftArray[i]<=rightArray[j])
			{
				mainArray[k] = leftArray[i];
				i++;
				
			}
			else{
				mainArray[k] = rightArray[j];
				j++;
			}
			k++;
		}
		
		while(i<nL)
		{
			mainArray[k] = leftArray[i];
			i++;
			k++;
		}
		
		while(j<nR)
		{
			mainArray[k] = rightArray[j];
			j++;
			k++;
		}

	}
	
	/*public static void main(String[] args) {
		int a[] ={1,5,8,9,7,2};
		
		a= performMergeSort(a);
		for(int ar:a)
			System.out.println(ar);
	}*/
}
