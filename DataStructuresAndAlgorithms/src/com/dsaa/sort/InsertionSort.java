package com.dsaa.sort;
/**
 * 
 * @author Akshay
 * This is Insertion sort algorithm .In this we consider the First element as the sorted array and the other half as the unsorted array.
 * and we keep on comparing the value of the unsorted array with the sorted array .The value greater than the value of the unsorted array are all shifted to the right hand side by 1.
 * By this at the end all elements are sorted.
 * 
 * Best Case : O(N)
 * Worst case is when all the digits are in reverse order 54321  .O(n^2).
 * This sorting algorithm is better than selection sort and bubble sort.
 */
public class InsertionSort {
	
	public int[] performInsertionSort(int[] arr)
	{
		int length=arr.length-1;
		for(int i=1;i<=length;i++){
			
			int value = arr[i];
			int hole=i;
			
			while(hole>0 && arr[hole-1]>value){
				arr[hole]=arr[hole-1];
				hole =hole-1;
			}
			
			arr[hole]=value;
		}
		
		return arr;
	}

}
